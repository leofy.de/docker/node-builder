FROM node:10-alpine
RUN apk update && apk upgrade
RUN apk -Uuv add make build-base groff python py-pip gcc curl xvfb bash git openssh zip libpng-dev
RUN pip install awscli
RUN npm install --global @hutson/semantic-delivery-gitlab speccy
ENTRYPOINT []
